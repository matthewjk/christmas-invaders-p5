// rotate drop when fired

function Drop(x, y) {
  this.x = x;
  this.y = y;
  this.r = 8;
  this.toDelete = false;
	this.dir = 10
	this.image = projectile

  this.show = function() {
		//push()
    noStroke();
    //fill(150, 0, 255);
		//rotate(random(0.01,0.1))
		//imageMode(CENTER)
    image(this.image, this.x, this.y, this.r * 2, this.r * 2);
		
    //ellipse(this.x, this.y, this.r*2, this.r*2);
		//pop()
  };

  this.evaporate = function() {
    this.toDelete = true;
  };

  this.hits = function(flower) {
    var d = dist(this.x, this.y, flower.x, flower.y);
		//print(d)
    if (d < this.r + flower.hitbox) {
      return true;
    } else {
      return false;
    }
  };

  this.move = function() {
    this.y = this.y - this.dir;
  };
}