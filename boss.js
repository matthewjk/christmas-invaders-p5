function Boss(x, y) {
  this.x = x;
  this.y = y;
  this.r = 150;
  this.lives = 100;
  this.hitbox = 150;
  //hitbox set to 80 for boss50 health
  this.xdir = 2;

	this.shiftDown = function() {
    this.xdir *= -1;
    this.y += 1;
  };

  this.show = function() {
    noStroke();
    imageMode(CENTER);
    // use this rect as a hitbox, make it shorter to accomodate change in image size
    //rect(this.x, this.y, this.r*2, this.hitbox*2);
    image(bossimg, this.x, this.y, this.r * 2, this.r * 2);
  };
}