function Flower(x, y) {
  this.x = x;
  this.y = y;
  this.r = 30;
  this.hitbox = 30;
  //set to either 1 or 3 for instant death or fall apart
  this.lives = 3;
  this.xdir = 5;
  this.image = [boss50,boss75,bossimg];

  this.grow = function() {
    this.lives = this.lives - 1;
  };

  this.shiftDown = function() {
    this.xdir *= -1;
    this.y += this.r;
  };

  this.move = function() {
    this.x = this.x + this.xdir;
  };

  this.show = function() {
    noStroke();
    imageMode(CENTER);
    image(this.image[this.lives-1], this.x, this.y, this.r * 2, this.r * 2);
  };
}