// increase speed at which enemies move
// give player another life each time they win
// score bonus at end of each round?

var ship, boss, wincondition;
var flowers = [];
var drops = [];
var snowflakes = [];
var shipx = -100;
var winnerx = -1000;
var bosswin = false;
var score = 0;
var lives = 3;
var level = 1;
var shotfreq = 0.9975;

function preload() {
  font = loadFont("assets/fonts/8bitfont.ttf");
  bg = loadImage("assets/images/christmas.jpg");
  enemy = loadImage("assets/images/enemy.png");
  projectile = loadImage("assets/images/projectile.png");
  shipimg = loadImage("assets/images/ship.png");
  shipwin = loadImage("assets/images/shipwin.png");
  winner = loadImage("assets/images/winner.png");
  bossimg = loadImage("assets/images/boss.png");
  boss75 = loadImage("assets/images/boss75.png");
  boss50 = loadImage("assets/images/boss50.png");
  boss25 = loadImage("assets/images/boss25.png");
  iceblast = loadSound("assets/audio/IceBlast.mp3");
  chiptune = loadSound("assets/audio/Chiptune.mp3");
  hit = loadSound("assets/audio/hit.mp3");
  shot = loadImage("assets/images/shot.png");
  pew = loadSound("assets/audio/pew.mp3");
  lose = loadSound("assets/audio/lose.mp3");
  loser = loadImage("assets/images/loser.png");
	levelup = loadSound("assets/audio/levelup.mp3")
}

function setup() {
  // potentially works better at 30fps than 60
  frameRate(30);
  chiptune.setVolume(0.1);
  chiptune.play();
  textFont(font);
  textSize(30);
  textAlign(LEFT, LEFT);
  var cnv = createCanvas(windowWidth, windowHeight);
  cnv.position(0,0)
  ship = new Ship();
  for (var i = 0; i < 14; i++) {
    l = [];
    for (var j = 0; j < 3; j++) {
      l.push(new Flower(i * 80, 60 + j * 80));
    }
    flowers.push(l);
  }
}

function newlevel(level) {
	levelup.setVolume(0.3);
  levelup.play();
  drops = [];
	flowers=[];
  if (lives < 3) {
    lives += 1;
  }
  ship = new Ship();
  if (level < 4) {
    shotfreq = 0.995 + 0.0015 * level;
    for (var i = 0; i < 14; i++) {
      l = [];
      for (var j = 0; j < 3; j++) {
        l.push(new Flower(i * 80, 60 + j * 80));
      }
      flowers.push(l);
    }
  } else if (level == 4) {
    boss = new Boss(windowWidth / 2, 180);
  }
}

function draw() {
  frameRate(30);
  imageMode(CORNER);
  background(bg);
  if (wincondition != undefined) {
    imageMode(CENTER);
    if (wincondition == false) {
      image(enemy, shipx, height * 0.5, 800, 600);
      image(loser, winnerx, height * 0.5, 1000, 400);
    } else if (wincondition == true) {
      image(shipwin, shipx, height * 0.5, 800, 600);
      image(winner, winnerx, height * 0.5, 1125, 400);
    }
    fill(255);
    text("Score " + score, 25, 50);
    text("Level " + level, 25, 100);
    text("Lives " + lives, 25, 150);
    shipx += 5;
    if (winnerx < width * 0.5) {
      winnerx += 5;
    }
  } else {
    ship.show();
    ship.move();
    fill(255);
    text("Score " + score, 25, 50);
    text("Level " + level, 25, 100);
    text("Lives " + lives, 25, 150);

    for (var i = 0; i < drops.length; i++) {
      if (drops[i].y > windowHeight || drops[i].y < 0) {
        drops[i].evaporate();
      }
      if (drops[i].hits(ship)) {
        lose.setVolume(0.5);
        lose.play();
        lives -= 1;
        if (lives <= 0) {
          wincondition = false;
        }
        frameRate(1);
        ship = new Ship();
        drops = []
				break
      }
      drops[i].show();
      drops[i].move();
      for (var j = 0; j < flowers.length; j++) {
        for (var x = 0; x < flowers[j].length; x++) {
          if (drops.length>0 && drops[i].hits(flowers[j][x])) {
            score += 1;
            hit.setVolume(0.5);
            hit.play();
            flowers[j][x].grow();
            drops[i].evaporate();
            if (flowers[j][x].lives == 0) {
              flowers[j][x].r = 0;
              if (flowers[j][x].r <= 0) {
                flowers[j].splice(x, 1);
                if (flowers.flat().length == 0) {
                  level += 1;
                  frameRate(1);
                  newlevel(level);
                  break;
                }
              }
            }
          }
        }
      }
    }

    var edge = false;
    for (var i = 0; i < flowers.length; i++) {
      for (var j = 0; j < flowers[i].length; j++) {
        flowers[i][j].show();
        flowers[i][j].move();
        if (flowers[i][j].x > width || flowers[i][j].x < 0) {
          edge = true;
        }
      }
      //closer to 0 == harder, bearing in mind 30*14 rolls each second
      if (random(1) > 0.9975) {
        if (flowers[i].length) {
          var drop = new Drop(
            flowers[i][flowers[i].length - 1].x,
            flowers[i][flowers[i].length - 1].y + 75
          );
          pew.setVolume(0.2);
          pew.play();

          drop.dir = drop.dir * -1;
          drop.image = shot;
          drops.push(drop);
        }
      }
    }

    if (level == 4) {
      boss.show();
      boss.shiftDown();
      for (var i = drops.length - 1; i >= 0; i--) {
        if (drops[i].hits(boss)) {
          hit.setVolume(0.5);
          hit.play();
          score += 1;
          drops[i].evaporate();
          boss.lives -= 1;
        }
        // add different health images here
        switch (true) {
          case boss.lives <= 0:
            wincondition = true;
            break;
          case boss.lives < 75 && boss.lives > 50:
            bossimg = boss75;
            break;
          case boss.lives < 50 && boss.lives > 25:
            // change boss hitbox size in order to accomodate smaller image
						boss.hitbox = 80;						
            bossimg = boss50;
            break;
          case boss.lives < 25:
            bossimg = boss25;
            break;
        }
      }
    }

    if (edge) {
      for (var i = 0; i < flowers.length; i++) {
        for (var x = 0; x < flowers[i].length; x++) {
          flowers[i][x].shiftDown();
          if (flowers[i][x].y >= windowHeight - 150) {
            wincondition = false;
          }
        }
      }
    }

    for (var i = drops.length - 1; i >= 0; i--) {
      if (drops[i].toDelete) {
        drops.splice(i, 1);
      }
    }
  }

  let t = frameCount / 60;
  for (var i = 0; i < random(5); i++) {
    snowflakes.push(new snowflake());
  }

  for (let flake of snowflakes) {
    flake.update(t);
    flake.display();
  }
}

function keyReleased() {
  if (key != " ") {
    ship.setDir(0);
  }
}

function keyPressed() {
  if (key === " ") {
    var drop = new Drop(ship.x - 40, ship.y - 60);
    drops.push(drop);
    var drop = new Drop(ship.x + 40, ship.y - 60);
    drops.push(drop);
    iceblast.setVolume(0.1);
    iceblast.play();
  }

  if (keyCode === RIGHT_ARROW) {
    ship.setDir(1);
  } else if (keyCode === LEFT_ARROW) {
    ship.setDir(-1);
  }
}