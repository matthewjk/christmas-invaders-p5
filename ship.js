function Ship() {
  this.x = width / 2;
	this.y = windowHeight-85
  this.xdir = 0;
	this.hitbox = 55

  this.show = function() {
    fill(255);
    imageMode(CENTER);
    image(shipimg, this.x, this.y, 100, 100);
  };

  this.setDir = function(dir) {
    this.xdir = dir;
  };

  this.move = function(dir) {
    this.x += this.xdir * 10;
  };
}