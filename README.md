# Christmas invaders p5

Play me on: https://www.openprocessing.org/sketch/637889

To play locally install `node.js`, `http-server` and `browser-sync`

Start game by navigating into it's directory and running `browser-sync start --server -f -w`

![screenshot](assets/images/screenshot.png)
